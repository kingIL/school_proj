import socket
from vpn_mokup import *

# login
class Client:
    def __init__(self, server_addr):
        self.rooms_data = {}
        self.rooms = []
        self.name = None
        self.vir_ip = ''

        self.email = 'newt@gmail.com'
        self.password = 'newt123'
        self.sock = socket.socket()
        self.sock.connect(server_addr)
        self.sock.sendall(b'a')

    def singup(self):
        self.name = input('enter username: ')
        self.sock.sendall(str('01' + self.email + '!' + self.name + '!' + self.password + '!' + self.password).encode())
        ret_c = self.sock.recv(3).decode()
        if ret_c[2] == 's':
            ret_msg = self.sock.recv(128).decode().split('!')
            print('successful new user \nwith email ' + ret_msg[0] + ' and ip ' + ret_msg[1])
            self.vir_ip = ret_msg[1]
        else:
            print('unsuccessful new user\n' + self.sc.recv(128).decode())

    def login(self):
        login_msg = "02" + self.email + '!' + self.password
        self.sock.sendall(login_msg.encode())
        data = self.sock.recv(3).decode()
        if data[2] == 's':
            size = int(self.sock.recv(3).decode())
            data = self.sock.recv(size).decode().split('!')
            self.name = data[0]
            self.vir_ip = data[1]
            self.rooms += eval(data[2])
            self.get_rooms_data()
        else:
            err_msg = self.sock.recv(512).decode()
            print('ERROR: ' + err_msg)

    def __get_room(self, room_name):
        req = '18' + room_name
        self.sock.sendall(req.encode())
        res = self.sock.recv(3).decode()
        if res[2] == 's':
            s = int(self.sock.recv(3).decode())
            data = self.sock.recv(s).decode().split('#') + ['{}']
            data = [eval(i) for i in data[:2]]
        else:
            return 'f' + self.sock.recv(128).decode()
        return 's', data[0]

    def get_rooms_data(self):
        err_rooms = []
        for room in self.rooms:
            self.sock.sendall(str('18' + room).encode())
            res = self.sock.recv(3).decode()
            if res[2] == 's':
                s = int(self.sock.recv(3).decode())
                data = self.sock.recv(s).decode().split('#') + ['{}']
                self.rooms_data[room] = [eval(i) for i in data[:2]]
            else:
                err_rooms += [room]
        if len(err_rooms):
            print('cant get data of rooms: ', *err_rooms)

    def add_room(self, room_name, passw=''):
        room_data = '#'.join([room_name, self.vir_ip, passw, str(True)])
        req = '10' + str(len(room_data)).rjust(3, '0') + room_data
        self.sock.sendall(req.encode())
        res = self.sock.recv(512).decode()

        if res[2] == 's':
            self.rooms_data[room_name] = [self.my_ip]
            self.rooms.append(room_name)
            print("new room created\nname: " + room_name + '\nadmin: ' + self.my_ip)
        else:
            print("ERROR: creat room-  " + res[3:])

    def join_room(self, room_name, passw=''):
        data = '11' + room_name + '#' + passw
        self.sock.sendall(data.encode())
        res = self.sock.recv(512).decode()
        if res[2] == 's':
            res_d, room_mems = self.__get_room(room_name)
            if res_d == 's':
                self.rooms.append(room_name)
                self.rooms[room_name] = room_mems
                print('Room data\nname: ' + room_name + '\nmembers: ' + room_mems)
            else:
                print("joined room but get data failed")
        else:
            print("ERROR: join room-  " + res[3:])

    def __str__(self):
        str_list = ['login parameters\nEmail:', self.email,' -  Password:', self.password, '\n']
        str_list += ['user data:\nname -', self.name,'\nip -', self.vir_ip, '\nrooms -', *self.rooms,'\n']
        str_list.append("rooms data:\n")
        for r, rd in self.rooms_data.items():
            str_list.append(str(r)+' - '+str(rd)+'\n')
        final_str = ''
        for s in str_list:
            if s[-1] != '\n':
                final_str += (s+' ')
            else:
                final_str += s
        return final_str

    def start_vpn(self):
        p = proxy(self.vir_ip, self.rooms_data)
        p.connect_to_server()
        opt = input('play as client or as server (c\\s)')
        try:
            if opt[0] == 'c':
                dst_ip = input("enter dst ip: ")
                dst_port = int(input("enter dst port: "))
                p.three_way_hs_client(dst_ip, dst_port)
            elif opt[0] == 's':
                p.three_way_hs_server()
        except Exception as e:
            print(e)
        return p

if __name__ == '__main__':
    client = Client(('127.0.0.1', 10000))
    client.singup()
    print(client, '\n\n')
    proxy = client.start_vpn()
    client.sock.close()