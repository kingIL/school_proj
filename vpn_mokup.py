from scapy.all import *
import socket
from random import randint
server_conn = ("127.0.0.1", 10000)

class proxy:
    def __init__(self, ip, rooms_mems):
        self.con = socket.socket()
        self.v_ip = ip
        self.room_mems = rooms_mems

    def connect_to_server(self):
        self.con.connect(server_conn)
        self.con.sendall(b'p'+ str(len(self.v_ip)).encode() + self.v_ip.encode())
    
    def send_pkt(self, pkt):
        pkt[IP].src = self.v_ip
        raw_pkt = str(pkt).encode()
        headers = (pkt[IP].src + '-' + pkt[IP].dst + ':').encode()
        size = str(len(headers + raw_pkt)).rjust(4, '0')
        vpn_p = size.encode() + headers + raw_pkt
        print('send packet:',  pkt.summary())
        self.con.sendall(vpn_p)
    
    def get_pkt(self):
        self.con.settimeout(5)
        try:
            size = int(self.con.recv(4).decode())
            pkt = self.con.recv(size).decode()
            pkt = Ether(eval(pkt))
            print("get packet from: " + pkt[IP].src)
            print(pkt.summary())
            return pkt
        except socket.timeout as e:
            print('not found packet')
        raise Exception("there is no connection over vpn")

    def creat_syn_pkt(self, dst_ip, dp):
        eth = Ether(src="ac:3b:77:59:f8:da", dst="fa:28:0d:d2:95:52")
        ip=IP(src=self.v_ip ,dst=dst_ip)
        SYN=TCP(sport=randint(10001,65535),dport=dp,flags='S',seq=randint(19999, 543210))
        pkt = eth/ip/SYN
        return pkt

    def creat_S_plus_A_pkt(self, dst_ip, sp, dp, A_section):
        eth = Ether(src="ac:3b:77:59:f8:da", dst="fa:28:0d:d2:95:52")
        ip=IP(src=self.v_ip ,dst=dst_ip)
        SA=TCP(sport=sp,dport=dp, flags='SA',seq=randint(19999, 543210),ack=A_section)
        pkt = eth/ip/SA
        return pkt

    def creat_ack_pkt(self, dst_ip, sp, dp, S_section, A_section, msg=''):
        eth = Ether(src="ac:3b:77:59:f8:da", dst="fa:28:0d:d2:95:52")
        ip=IP(src=self.v_ip ,dst=dst_ip)
        ack=TCP(sport=sp,dport=dp,flags='A',seq=S_section,ack=A_section)
        if msg:
            pkt = eth/ip/ack/Raw(load = msg)
        else:
            pkt = eth/ip/ack
        return pkt

    def three_way_hs_client(self, to_ip, to_port):
        """3 way hand-shake client side
            need syn and ack pkts
        Args:
            to_ip ([type]): [dst ip]
            to_port ([type]): [ds port]
        """
        syn_p = self.creat_syn_pkt(to_ip, to_port)
        self.send_pkt(syn_p)
        SpA_pkt = self.get_pkt()
        ack_pkt = self.creat_ack_pkt(SpA_pkt[IP].src, SpA_pkt[TCP].dport, SpA_pkt[TCP].sport, SpA_pkt[TCP].ack, SpA_pkt[TCP].seq+1)
        self.send_pkt(ack_pkt)
        print('send ack packet\nfinish 3 way hand shake')

    def three_way_hs_server(self):
        """3 way hand-shake server side
            need only syn+ack pkt
        """
        syn_pkt = self.get_pkt()
        SplusA_pkt = self.creat_S_plus_A_pkt(syn_pkt[IP].src, syn_pkt[TCP].dport, syn_pkt[TCP].sport, syn_pkt[TCP].seq+1)
        self.send_pkt(SplusA_pkt)
        ack_pkt = self.get_pkt()
        print("get ack packet\nfinish 3 way hand shake")

def main_action():
    my_ip = '25.200.0.' + input('enter your ip: 25.200.0.(num from 10 to 254)')
    p = proxy(my_ip,['25.200.0.'+str(x) for x in range(11,15)])
    p.connect_to_server()
    opt = input('play as client or as server (c\\s)')
    if opt[0] == 'c':
        dst_ip = input("enter dst ip: ")
        dst_port = int(input("enter dst port: "))
        p.three_way_hs_client(dst_ip, dst_port)
    elif opt[0] == 's':
        p.three_way_hs_server()
    return p


if __name__ == '__main__':
    p = main_action()
    opt = input('MENU:\nget msg - gm\nsend msg - sm')
    if opt == 'sm':
        msg = input('enter msg: ')
    elif opt == 'gm':
        pkt = p.get_pkt()